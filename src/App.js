import logo from './logo.svg';
import './App.css';
import { Route, Link } from "react-router-dom";
import Front from "./views/front.js"
import Second from "./views/second.js"
import Third from "./views/third.js"

function App() {
  return (
    <div className="App">
      
      <div>
        <Route exact path='/' component={Front} />
        <Route path='/second' component={Second} />
        <Route path='/third' component={Third} />
      </div>

    </div>
  );
}

export default App;
