import React, {useEffect, useState} from "react";


function Front() {


  const [count, setCount] = useState(parseInt(localStorage.getItem("count")) || 0);

useEffect(() => {
  setCount((prev) => prev + 1)
  let saveCount = count + 1;
  localStorage.setItem("count", saveCount )
},[])

  return(
    <div>
      <h1>Yes yes, this was updated from the repo and was triggered by pushing to gitlab, preatty neat!</h1>
      <h1>Count: {count}</h1>
    </div>
  )
}

export default Front;
